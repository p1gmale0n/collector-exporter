from celery.app.control import Inspect
from celery import Celery
from prometheus_client import start_http_server
from prometheus_client.core import GaugeMetricFamily, REGISTRY, CounterMetricFamily
import random
import redis
import environ
import time

env = environ.Env()
QUEUES = env.list('QUEUES')

class RedisCollector(object):
    def __init__(self):
        pass

    def collect(self, queue_name=QUEUES):
        client = redis.StrictRedis.from_url(
            env('REDIS_URL', default='redis://localhost:6379')
        )

        gQueue = GaugeMetricFamily("redis_queue_size",
                                   'Number of items per celery queue', labels=['queue'])
        gRunning = GaugeMetricFamily("redis_running_size",
                                     'Number of running jobs per routing_key', labels=['routing_key'])

        for queue in QUEUES:
            gQueue.add_metric([queue], sum([client.llen(queue)]))
        yield gQueue

        dictOfQueues = {i: 0 for i in QUEUES}
        active_tasks = self.getActiveTasks()
        for _, tasks in active_tasks.items():
            for task in tasks:
                dictOfQueues[task.get("delivery_info").get("routing_key")] += 1
        for queue in dictOfQueues:
            gRunning.add_metric([queue], dictOfQueues[queue])
        yield gRunning

    def getActiveTasks(self):
        try:
            active_tasks = Celery(
                broker=env('REDIS_URL', default='redis://localhost:6379')).control.inspect().active()
            if not active_tasks:
                active_tasks = {}
        except IOError as e:
            from errno import errorcode
            print("Error connecting to the backend: " + str(e))
        return active_tasks

if __name__ == '__main__':
    # Start up the server to expose the metrics.
    start_http_server(8000)
    REGISTRY.register(RedisCollector())
    while True:
        time.sleep(1)
