FROM python:3.8-alpine3.12

ENV PYTHONUNBUFFERED 1

WORKDIR /app
COPY requirements.txt requirements.txt
RUN pip3 install -r requirements.txt

COPY server.py server.py

CMD [ "python3", "/app/server.py" ]
